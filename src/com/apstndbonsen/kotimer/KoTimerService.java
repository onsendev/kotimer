package com.apstndbonsen.kotimer;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.widget.RemoteViews;

import com.apstndbonsen.kotimer.dao.DBManager;
import com.apstndbonsen.kotimer.list.KoTimer;
import com.apstndbonsen.kotimer.list.TimerListAdapter;
import com.apstndbonsen.kotimer.util.KoAlarmManager;
import com.apstndbonsen.kotimer.util.StringManager;

@SuppressLint("UseSparseArrays")
public class KoTimerService extends IntentService {
	
	private static HashMap<Long, KoTimer> notificationMap = null;
	private static HashMap<Long, Notification.Builder> showingNotificationMap = null;
	private DBManager dbManager = null;
	public static ArrayList<KoTimer> nowMaster = null;
	public static boolean allStop = true;
	private Handler handler = new Handler();
	private static PendingIntent alarmPendingIntent = null;
	private static Intent alarmIntent = null;
	private static AlarmManager alarmManager = null;

	
    public static NotificationManager notificationManager;


	public KoTimerService(String name) {
		super(name);
	}
	public KoTimerService(){
		super("KoTimerService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		
		if (alarmManager == null) {
		    alarmManager = (AlarmManager)getBaseContext().getSystemService(ALARM_SERVICE);
		}
		
		if (alarmIntent == null) {
			alarmIntent = new Intent(getBaseContext(), TimerListActivity.class);
			alarmPendingIntent = PendingIntent.getActivity(getBaseContext(), 15, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
		}

		
		if (showingNotificationMap == null) {
			showingNotificationMap = new HashMap<Long, Notification.Builder>();
		}
		
		dbManager = DBManager.getInstance();
		if (notificationManager == null) {
			notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
		}

		
		nowMaster = null;
		if (dbManager == null) {
			return;
		}
		
		try {
			nowMaster = dbManager.selectAll();
		} catch (Throwable e) {
		}
		
		TimerListAdapter.timers = nowMaster;

		allStop = true;
		for (KoTimer koTimer : nowMaster) {
			//走っている
			if (koTimer.isRunning()) {
				allStop = false;
				
				if (koTimer.getRemainSec() <= 0) {
					koTimer.setRemainSec(0);
					//ならす
					KoAlarmManager.startAlarm(koTimer.getId(), koTimer, getApplicationContext());
					alarmManager.cancel(alarmPendingIntent);
				}else{
					koTimer.decrementRemainSec();
				}
				dbManager.update(koTimer);
			}
		}		

		//ボタン押せなくしちゃう
		handler.post(new Runnable() {
			public void run() {
				if (TimerListActivity.btnAddTimer != null) {
					if (allStop) {
						TimerListActivity.btnAddTimer.setEnabled(true);
					}else{
						TimerListActivity.btnAddTimer.setEnabled(false);
					}
				}
			}
		});
		
		notificationMap = new HashMap<Long, KoTimer>();
		for (KoTimer koTimer : nowMaster) {
			//走っているか、一時停止中（残り時間が変更されてる）
			if (koTimer.isRunning()) {
				notificationMap.put(getNotificationId(koTimer.getId()), koTimer);
			}
		}
		
		for (Map.Entry<Long, KoTimer> entry : notificationMap.entrySet()) {
			if (!showingNotificationMap.containsKey(entry.getKey())) {
				final int notificationId = (int)(long)entry.getKey();
//				final KoTimer notificationKoTimer = entry.getValue();
			    Notification.Builder builder = new Notification.Builder(getApplicationContext())
			    .setSmallIcon(R.drawable.kotimer_icon_200)
				.setContentTitle(" ")
				.setWhen(System.currentTimeMillis());
				showingNotificationMap.put((long)notificationId, builder);
				
			    new NotificationUpdateThread(notificationId, builder).start();
			}else{
			}
		}		
	}
	
	
	private class NotificationUpdateThread extends Thread{
		
		private Notification.Builder mBuilder = null;
		private long notificationId = 0;
		
		public NotificationUpdateThread(long notificationId, Notification.Builder builder){
			this.mBuilder = builder;
			this.notificationId = notificationId;
		}
		
		@Override
		public void run(){
			RemoteViews contentView = new RemoteViews(getPackageName(), R.layout.row_notification_running);
			dbManager = DBManager.getInstance();
			for (int i = 0; i <= 60; i++) {
				
				KoTimer myKoTimer = dbManager.select(getDBId(notificationId));
				
				if (myKoTimer == null) {
					return;
				}
				
				if (!myKoTimer.isRunning()) {
					notificationManager.cancel((int)notificationId);
					showingNotificationMap.remove(notificationId);
					return;
				}
				
			    contentView.setTextViewText(R.id.tvTimerName, myKoTimer.getAlarmName());
				Calendar calendar = Calendar.getInstance();
				calendar.add(Calendar.SECOND, (int)myKoTimer.getRemainSec());
				contentView.setTextViewText(R.id.tvEndTime, StringManager.formatZero(calendar.get(Calendar.HOUR)) + ":" + StringManager.formatZero(calendar.get(Calendar.MINUTE)));
			    
			    contentView.setTextViewText(R.id.tvRemain, StringManager.formatMinuteSecBySec(myKoTimer.getRemainSec()));
				
			    if (nowMaster == null) {
					continue;
				}
			    
			    Intent intent = new Intent(getApplicationContext(), TimerListActivity.class);
				for (int j = 0; j < nowMaster.size(); j++) {
					if (notificationId == nowMaster.get(j).getId()) {
						KoTimer koTimer = nowMaster.get(j);
						if (koTimer.isRunning()) {
							intent.setAction("Pause"+notificationId);
							intent.putExtra("playOrPause", "Pause");
//							contentView.setImageViewResource(R.id.ivPlayPause, R.drawable.img_pause);
						}else{
							intent.setAction("Play"+notificationId);
							intent.putExtra("playOrPause", "Play");
//							contentView.setImageViewResource(R.id.ivPlayPause, R.drawable.img_play);
						}
					}
				}
				intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
				PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), (int)notificationId, intent, PendingIntent.FLAG_UPDATE_CURRENT);
				
				mBuilder.setContentIntent(pendingIntent);
				mBuilder.setSmallIcon(R.drawable.kotimer_icon_200);
				mBuilder.setContent(contentView);
				try {
					Thread.sleep(900);
					Notification notification = mBuilder.build();
					notification.flags = Notification.FLAG_NO_CLEAR;
					notificationManager.notify((int)notificationId, mBuilder.build());
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
				if (i==59) {
					i = 0;
				    mBuilder = new Notification.Builder(getApplicationContext())
					.setWhen(System.currentTimeMillis());
					mBuilder.setContentIntent(pendingIntent);
					mBuilder.setSmallIcon(R.drawable.kotimer_icon_200);
					mBuilder.setContent(contentView);
				}
			}
		}
	}
	
	public static synchronized long getNotificationId(long dbId){
		return dbId + 53544150;
	}
	public static synchronized long getDBId(long notificationId){
		return notificationId - 53544150;
	}



}
