package com.apstndbonsen.kotimer;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

public class NotificationClickService extends Service {

	@Override
	public IBinder onBind(Intent intent) {
        String playOrPause = intent.getStringExtra("playOrPause");
        Bundle extras = intent.getExtras();
//        System.out.println(startId);
        playOrPause = extras.getString("playOrPause");
        Log.i("Service", playOrPause);
		return null;
	}
	
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

}
