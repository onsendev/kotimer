package com.apstndbonsen.kotimer;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.apstndbonsen.kotimer.dao.DBManager;
import com.apstndbonsen.kotimer.list.KoTimer;
import com.apstndbonsen.kotimer.list.TimerListAdapter;
import com.apstndbonsen.kotimer.util.KoAlarmManager;
import com.apstndbonsen.kotimer.util.TypeFaceSetter;

public class TimerListActivity extends Activity {

	private ListView lvTimer = null;
	private NotificationManager notificationManager;
    private Notification.Builder builder;
    private TimerListAdapter adapter = null;
    private Handler handler = new Handler();
    public static Button btnAddTimer = null;
    private ArrayList<KoTimer> timers = null;

    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		DBManager dbManager = new DBManager(getApplicationContext(), "KoTimer", null, 1);
		DBManager.thisInstance = dbManager;
		
		getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_timer_list);
		new TypeFaceSetter().setTypeFace(this);
		lvTimer = (ListView)findViewById(R.id.lvTimer);
		
		//ベースとなるタイマーを起動
		Context context = getBaseContext();
	    Intent intent = new Intent(context, KoTimerService.class);
	    PendingIntent pendingIntent = PendingIntent.getService(context, 50, intent, PendingIntent.FLAG_UPDATE_CURRENT);
	    AlarmManager alarmManager = (AlarmManager)context.getSystemService(ALARM_SERVICE);
	    alarmManager.setInexactRepeating(AlarmManager.RTC, System.currentTimeMillis(), 1000, pendingIntent);
	    
	    btnAddTimer = (Button)findViewById(R.id.btnAddTimer);

	}
	
	@Override
	protected void onResume(){
		super.onResume();
		
		final DBManager manager = DBManager.getInstance();
		timers = manager.selectAll();
		
		for (KoTimer koTimer2 : timers) {
			System.out.println(koTimer2.getAlarmName() + "     seconds " + koTimer2.getAlarmSeconds());
		}
		
		TimerListAdapter adapter = new TimerListAdapter(getApplicationContext(), 0, timers);
		lvTimer.setAdapter(adapter);
		this.adapter = adapter;
		
		new Timer().schedule(new TimerTask() {
			@Override
			public void run() {
				handler.post(new Runnable() {
					public void run() {
						TimerListActivity.this.adapter.notifyDataSetChanged();
					}
				});
			}
		}, 0, 1000);
		
		lvTimer.setClickable(true);
	    //選択された
	    lvTimer.setOnItemClickListener(new OnItemClickListener() {
	    	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
	    		//アラームが鳴っているときはとめる
	    		if (KoTimerService.nowMaster != null) {
	    			KoTimer koTimer = KoTimerService.nowMaster.get(position);
	    			if (koTimer.getRemainSec() <= 1 && koTimer.isRunning()) {
						KoAlarmManager.stopAlarm(koTimer.getId());
						koTimer.setRunning(false);
						koTimer.setRemainSec(koTimer.getAlarmSeconds());
						DBManager.thisInstance.update(koTimer);
						return;
					}
	    			if (!koTimer.isRunning()) {
	    				Intent intent = new Intent(getApplicationContext(), TimerSettingActivity.class);
	    				intent.putExtra(TimerSettingActivity.KEY_EDITTARGETID, koTimer.getId());
						startActivity(intent);
					}
				}
	    	}
    	});
	    lvTimer.setOnItemLongClickListener(new OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
				final KoTimer koTimer = KoTimerService.nowMaster.get(position);
				if (koTimer.isRunning()) {
					Toast.makeText(TimerListActivity.this, "タイマーを終了してから操作してください", Toast.LENGTH_LONG).show();
					return false;
				}
				final String[] items = {"リセット", "削除"};
				final AlertDialog.Builder builder = new AlertDialog.Builder(TimerListActivity.this);
				builder.setItems(items, new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						switch (which) {
							case 0:
								koTimer.setRemainSec(koTimer.getAlarmSeconds());
								manager.update(koTimer);
								timers = manager.selectAll();
								break;
							case 1:
								manager.delete(koTimer.getId());
								timers = manager.selectAll();
								TimerListAdapter adapter = new TimerListAdapter(getApplicationContext(), 0, timers);
								lvTimer.setAdapter(adapter);
								break;
							default:
								break;
						}
					}
				})
				.setTitle(koTimer.getAlarmName()).create().show();;
				return false;
			}
		});
	}
	
	public void onAddTimerClick(View view){
		startActivity(new Intent(getApplicationContext(), TimerSettingActivity.class));
	}
}

//再生、一時停止
