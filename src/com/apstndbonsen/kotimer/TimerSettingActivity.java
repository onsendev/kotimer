package com.apstndbonsen.kotimer;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.apstndbonsen.kotimer.dao.DBManager;
import com.apstndbonsen.kotimer.list.KoTimer;
import com.apstndbonsen.kotimer.util.TypeFaceSetter;

public class TimerSettingActivity extends Activity {

	public static final String KEY_EDITTARGETID = "editTargetId";
	
	private DBManager dbManager = null;
	private String timerName = null;
	
	private EditText etByou = null;
	private EditText etFun = null;
	private EditText etAlarmName = null;
	private RadioButton rbSound = null;
	private RadioButton rbVibration = null;
	private RadioButton rbLaunchApp = null;
	private RadioButton rbLaunchNotification = null;
	private CheckBox cbLed = null;
	private CheckBox cbRunningNotification = null;
	
	private long editTargetId = 0;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_timer_add);
		new TypeFaceSetter().setTypeFace(this);
		
		editTargetId = getIntent().getLongExtra(KEY_EDITTARGETID, -1);
	}
	
	@Override
	protected void onResume(){
		super.onResume();
		
		dbManager = new DBManager(getApplicationContext(), "KoTimer", null, 1);
		ArrayList<KoTimer> timers = dbManager.selectAll();
		
		timerName = "Timer" + (timers.size()+1);
		
		etByou = (EditText)findViewById(R.id.etByou);
		etFun = (EditText)findViewById(R.id.etFun);
		etAlarmName = (EditText)findViewById(R.id.etTimerName);
		rbSound = (RadioButton)findViewById(R.id.rbSound);
		rbVibration = (RadioButton)findViewById(R.id.rbViblate);
		cbLed = (CheckBox)findViewById(R.id.cbLed);
		rbLaunchApp = (RadioButton)findViewById(R.id.rbLaunchActivity);
		rbLaunchNotification = (RadioButton)findViewById(R.id.rbLaunchNotification);
		cbRunningNotification = (CheckBox)findViewById(R.id.cbRunning);

		etAlarmName.clearFocus();
		etFun.clearFocus();
		etByou.clearFocus();

		//編集モード
		if (editTargetId != -1) {
			KoTimer koTimer = dbManager.select(editTargetId);
			if (koTimer != null) {
				timerName = koTimer.getAlarmName();
				etByou.setText(String.valueOf(koTimer.getAlarmSeconds() % 60));
				etFun.setText(String.valueOf(koTimer.getAlarmSeconds() / 60));
				etAlarmName.setText(koTimer.getAlarmName());
				rbSound.setChecked(koTimer.isSound());
				rbVibration.setChecked(koTimer.isVibrate());
				cbLed.setChecked(koTimer.isLed());
				rbLaunchApp.setChecked(koTimer.isLaunchActivity());
				rbLaunchNotification.setChecked(!koTimer.isLaunchActivity());
				cbRunningNotification.setChecked(koTimer.isRunningNotification());
			}
		}
		etAlarmName.setHint(timerName);
	}
	
			
	public void onSaveClick(View view){
		if(!validationCheck()){
			return;
		}
		insertTimer(false);
		finish();
	}
	public void onSaveAndStartClick(View view){
		if(!validationCheck()){
			return;
		}
		insertTimer(true);
		finish();
	}

	private boolean validationCheck(){
		
		if (etByou.getText().toString().length() == 0) {
			if (etFun.getText().toString().length() > 0) {
				etByou.setText("0");
			}else{
				Toast.makeText(getApplicationContext(), "時間を入力してください", Toast.LENGTH_LONG).show();
				return false;
			}
		}
		if (etFun.getText().toString().length() == 0) {
			etFun.setText("0");
		}
		if (etFun.getText().toString().equals("0") && etByou.getText().toString().equals("0")) {
			Toast.makeText(getApplicationContext(), "時間を入力してください", Toast.LENGTH_LONG).show();
			return false;
		}
		if (Integer.valueOf(etByou.getText().toString()) >= 60) {
			Toast.makeText(getApplicationContext(), "60秒未満で入力してください", Toast.LENGTH_LONG).show();
			return false;
		}
		return true;
	}
	
	private void insertTimer(boolean autoStart){
		KoTimer record = new KoTimer();
		
		if (etAlarmName.getText().toString().length() == 0) {
			record.setAlarmName(timerName);
		}else{
			record.setAlarmName(etAlarmName.getText().toString());
		}
		
		record.setLaunchActivity(rbLaunchApp.isChecked());
		record.setRunningNotification(cbRunningNotification.isChecked());
		
		int fun = Integer.valueOf(etFun.getText().toString());
		int byou = Integer.valueOf(etByou.getText().toString());
		long sum = fun * 60 + byou;
		record.setAlarmSeconds(sum);
		
		record.setLed(cbLed.isChecked());
		record.setSound(rbSound.isChecked());
		record.setVibrate(rbVibration.isChecked());
		record.setRemainSec(sum);
		if (autoStart) {
			record.setRunning(true);
		}

		//編集？
		if (editTargetId != -1) {
			record.setId(editTargetId);
			dbManager.update(record);
		}else{
			dbManager.insert(record);
		}
	}
	
}
