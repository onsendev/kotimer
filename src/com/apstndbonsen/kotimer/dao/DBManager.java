package com.apstndbonsen.kotimer.dao;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;

import com.apstndbonsen.kotimer.list.KoTimer;


public class DBManager extends DBOpenHelper{

	private SQLiteDatabase sqLiteDB = null;
	public static int DB_VERSION = 1;
	public static DBManager thisInstance = null;

	public DBManager(Context context, String name, CursorFactory factory, int version) {
		super(context, name, factory, version);
	}
	
	public static DBManager getInstance(){
		return thisInstance;
	}
	public static void setInstance(DBManager dbManager){
		thisInstance = dbManager;
	}
	
	public synchronized ArrayList<KoTimer> selectAll(){
		ArrayList<KoTimer> array = new ArrayList<KoTimer>();
		Cursor c = null;
		long start = System.currentTimeMillis();

		try{
			System.out.println("-------------------");
			System.out.println("selectAll called");

			sqLiteDB = this.getReadableDatabase();
			/*
				
				SQLiteDatabase.query()メソッドの第一引数はテーブル名です。
				第二引数は、取得する列名(カラム名、フィールド名)の配列を指定します。
				第三引数、第四引数は取得するレコードの条件を指定します。
				今回は、bookmark列の文字列に、「android」という文言が含まれる文字列、という条件にしています。
				第五引数は、group by句を指定します。
				第六引数は、Having句を指定します。
				第七引数は、order by句を指定します。
				第八引数は、limit句(取得するレコードの上限数)を指定します。
				使わない場合は、nullを指定します。
								
			 */

			
			c = sqLiteDB.query("KoTimer",
							null,
							null,
							null,
							null,
							null,
							null);
			
			int koTimerIdIndex		= c.getColumnIndexOrThrow("_id");
			int alarmSecondIndex	= c.getColumnIndexOrThrow("alarmSecond");
			int soundIndex			= c.getColumnIndexOrThrow("sound");
			int ledIndex			= c.getColumnIndexOrThrow("led");
			int vibrateIndex		= c.getColumnIndexOrThrow("vibrate");
			int alarmNameIndex		= c.getColumnIndexOrThrow("alarmName");
			int startTimeIndex		= c.getColumnIndexOrThrow("startTime");
			int launchActivityIndex		= c.getColumnIndexOrThrow("launchActivity");
			int runningNotificationIndex		= c.getColumnIndexOrThrow("runningNotification");
			int isRunningIndex		= c.getColumnIndexOrThrow("isRunning");
			int remainSecIndex		= c.getColumnIndexOrThrow("remainSec");

			int numRows = c.getCount();
			c.moveToFirst();


			for( int i = 0; i < numRows; i++ ){
				KoTimer record = new KoTimer();
				record.setId(c.getLong(koTimerIdIndex));
				record.setAlarmName(c.getString(alarmNameIndex));
				record.setAlarmSeconds(c.getLong(alarmSecondIndex));
				record.setLed(c.getString(ledIndex));
				record.setSound(c.getString(soundIndex));
				record.setStartTime(c.getLong(startTimeIndex));
				record.setVibrate(c.getString(vibrateIndex));
				record.setLaunchActivity(c.getString(launchActivityIndex));
				record.setRunningNotification(c.getString(runningNotificationIndex));
				record.setRunning(c.getString(isRunningIndex));
				record.setRemainSec(c.getLong(remainSecIndex));

				array.add(record);
				c.moveToNext();
			}
	
		}catch(Throwable e){
			e.printStackTrace();
			return new ArrayList<KoTimer>();
		}finally{
			if(c != null)
				c.close();
			if(sqLiteDB != null)
				sqLiteDB.close();
		}

		System.out.println("selectAll finish " + (System.currentTimeMillis() - start) + "mSec");
		System.out.println("-------------------");

		return array;

	}
	
	public synchronized KoTimer select(long id){
		ArrayList<KoTimer> timers = selectAll();
		for (KoTimer koTimer : timers) {
			if (koTimer.getId() == id) {
				return koTimer;
			}
		}
		System.out.println("select called id = " + id);
		return null;
	}
	
	public synchronized void insert(KoTimer record){

		// 書き込み可能なDBをオープン
		sqLiteDB = this.getWritableDatabase();
		System.out.println("insert called");

		try{

			ContentValues val = new ContentValues();
			
			val.put("alarmSecond", record.getAlarmSeconds());
			val.put("sound", String.valueOf(record.isSound()));
			val.put("led", String.valueOf(record.isLed()));
			val.put("vibrate", String.valueOf(record.isVibrate()));
			val.put("alarmName", record.getAlarmName());
			val.put("startTime", record.getStartTime());
			val.put("launchActivity", String.valueOf(record.isLaunchActivity()));
			val.put("runningNotification", String.valueOf(record.isRunningNotification()));
			val.put("isRunning", String.valueOf(record.isRunning()));
			val.put("remainSec", record.getRemainSec());

			//insert
			sqLiteDB.insert("KoTimer", null, val);

		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(sqLiteDB != null){
				sqLiteDB.close();
			}
		}
	}
	
	public synchronized boolean update(KoTimer record){
		try{
			// 書き込み可能なDBをオープン
			sqLiteDB = this.getWritableDatabase();

			String sql = "update KoTimer set "
					+ "alarmSecond = " + record.getAlarmSeconds() + ", "
					+ "sound = '" + record.isSound() + "', "
					+ "led = '" + record.isLed() + "', "
					+ "vibrate = '" + record.isVibrate() + "', "
					+ "alarmName = '" + record.getAlarmName() + "', "
					+ "isRunning = '" + record.isRunning() + "', "
					+ "remainSec = " + record.getRemainSec() + ", "
					+ "launchActivity = '" + record.isLaunchActivity() + "', "
					+ "runningNotification = '" + record.isRunningNotification() + "', "
					+ "startTime = " + record.getStartTime()   
					+ " where _id = " + record.getId();
			sqLiteDB.execSQL(sql);

		}catch(Exception e){
			e.printStackTrace();
			return false;
		}finally{
			if(sqLiteDB != null){
				sqLiteDB.close();
			}
		}
		
		return true;

		
		
	}
	
	public synchronized boolean delete(long id){
		try{
			// 書き込み可能なDBをオープン
			sqLiteDB = this.getWritableDatabase();

			String sql = "delete from KoTimer where _id = " + id;
			sqLiteDB.execSQL(sql);

		}catch(Exception e){
			e.printStackTrace();
			return false;
		}finally{
			if(sqLiteDB != null){
				sqLiteDB.close();
			}
		}
		
		return true;

		
	}



}
