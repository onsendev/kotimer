package com.apstndbonsen.kotimer.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class DBOpenHelper extends SQLiteOpenHelper {

	public static final int DB_VERSION = 1;
	public static final String DB_NAME = "KoTimer";

	public static String getDBName(){
		return DB_NAME;
	}
	
	public DBOpenHelper(Context context ,String name, CursorFactory factory, int version){
		super(context, name, factory , version);
	}
	
	
	@Override
	public void onCreate(SQLiteDatabase db) {

		db.beginTransaction();
		String sql = null;
		
		try{
			sql = "CREATE TABLE KoTimer (_id INTEGER PRIMARY KEY AUTOINCREMENT,  alarmSecond INTEGER NOT NULL DEFAULT 300,  alarmName TEXT NOT NULL DEFAULT Alarm, startTime INTEGER DEFAULT 0, sound TEXT DEFAULT false, led TEXT NOT NULL DEFAULT false, vibrate TEXT NOT NULL DEFAULT false, launchActivity TEXT NOT NULL DEFAULT false, runningNotification TEXT NOT NULL DEFAULT false, remainSec INTEGER NOT NULL DEFAULT 0, isRunning TEXT NOT NULL DEFAULT false)";

			db.execSQL(sql);
			db.setTransactionSuccessful();

		}catch(Exception e){
			e.printStackTrace();
		}finally{
			db.endTransaction();
		}

		System.out.println("created db");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	}

}
