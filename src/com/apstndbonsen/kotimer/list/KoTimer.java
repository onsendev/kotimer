package com.apstndbonsen.kotimer.list;

import java.util.TimerTask;

public class KoTimer {

	private long id = 0;
	private long startTime = 0;
	private long alarmSeconds = 0;
	private long remainSec = 0;
	private boolean isRunning = false;
	private String alarmName = null;
	private boolean vibrate = false;
	private boolean led = false;
	private boolean sound = false;
	private boolean launchActivity = false;
	private boolean runningNotification = false;
	private TimerTask timerTask = null;
	
	public long getStartTime() {
		return startTime;
	}
	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}
	public long getAlarmSeconds() {
		return alarmSeconds;
	}
	public void setAlarmSeconds(long alarmSeconds) {
		this.alarmSeconds = alarmSeconds;
	}
	public String getAlarmName() {
		return alarmName;
	}
	public void setAlarmName(String alarmName) {
		this.alarmName = alarmName;
	}
	public boolean isVibrate() {
		return vibrate;
	}
	public void setVibrate(boolean vibrate) {
		this.vibrate = vibrate;
	}
	public void setVibrate(String vibrate) {
		this.vibrate = Boolean.valueOf(vibrate);
	}
	public boolean isLed() {
		return led;
	}
	public void setLed(boolean led) {
		this.led = led;
	}
	public void setLed(String led) {
		this.led = Boolean.valueOf(led);
	}
	public boolean isSound() {
		return sound;
	}
	public void setSound(boolean sound) {
		this.sound = sound;
	}
	public void setSound(String sound) {
		this.sound = Boolean.valueOf(sound);
	}
	public TimerTask getTimerTask() {
		return timerTask;
	}
	public void setTimerTask(TimerTask timerTask) {
		this.timerTask = timerTask;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public boolean isLaunchActivity() {
		return launchActivity;
	}
	public void setLaunchActivity(boolean launchActivity) {
		this.launchActivity = launchActivity;
	}
	public void setLaunchActivity(String launchActivity) {
		this.launchActivity = Boolean.valueOf(launchActivity);
	}
	public boolean isRunningNotification() {
		return runningNotification;
	}
	public void setRunningNotification(boolean runningNotification) {
		this.runningNotification = runningNotification;
	}
	public void setRunningNotification(String runningNotification) {
		this.runningNotification = Boolean.valueOf(runningNotification);
	}
	public long getRemainSec() {
		return remainSec;
	}
	public void setRemainSec(long remainSec) {
		this.remainSec = remainSec;
	}
	public boolean isRunning() {
		return isRunning;
	}
	public void setRunning(boolean isRunning) {
		this.isRunning = isRunning;
	}
	public void setRunning(String isRunning) {
		this.isRunning = Boolean.valueOf(isRunning);
	}
	public void decrementRemainSec(){
		this.remainSec--;
	}
}
