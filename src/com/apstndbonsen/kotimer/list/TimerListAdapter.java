package com.apstndbonsen.kotimer.list;

import java.util.ArrayList;
import java.util.Calendar;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.apstndbonsen.kotimer.KoTimerService;
import com.apstndbonsen.kotimer.R;
import com.apstndbonsen.kotimer.TimerListActivity;
import com.apstndbonsen.kotimer.dao.DBManager;
import com.apstndbonsen.kotimer.util.KoAlarmManager;
import com.apstndbonsen.kotimer.util.StringManager;
import com.apstndbonsen.kotimer.util.TypeFaceManager;

public class TimerListAdapter extends ArrayAdapter<KoTimer> {
	
	public static ArrayList<KoTimer> timers = null;
    private LayoutInflater inflater;
    private Typeface typeface;
    private Handler handler = new Handler();
	
    public TimerListAdapter(Context context, int resource, ArrayList<KoTimer> objects) {
		super(context, resource, objects);
		timers = objects;
        typeface = TypeFaceManager.getLightTypeface_001(getContext());
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	public View getView(final int position, View convertView, ViewGroup parent) {
        View view;
        if (convertView != null) {
            view = convertView;
        } else {
            view = inflater.inflate(R.layout.row_timer_list, null);
        }
        
        if (timers == null || timers.size() == 0) {
			return view;
		}
        
        final KoTimer timer = timers.get(position);
        
        final TextView tvTimerName = (TextView)view.findViewById(R.id.tvTimerName);
        final TextView tvRemain = (TextView)view.findViewById(R.id.tvRemain);
        final TextView tvEndTime = (TextView)view.findViewById(R.id.tvEndTime);
        final TextView tvMsgSyuuryouJikoku = (TextView)view.findViewById(R.id.tvMsgSyuuryouJikoku);
        final Button btnPlayPause = (Button)view.findViewById(R.id.btnPlayPause);
        
        tvTimerName.setText(timer.getAlarmName());
        
        //タイマーが動いていない
        if (!timer.isRunning()) {
			btnPlayPause.setBackgroundResource(R.drawable.img_play);
			tvEndTime.setText("--:--");
		}else{
			btnPlayPause.setBackgroundResource(R.drawable.img_pause);
			Calendar calendar = Calendar.getInstance();
			calendar.add(Calendar.SECOND, (int)timer.getRemainSec());
			tvEndTime.setText(StringManager.formatZero(calendar.get(Calendar.HOUR_OF_DAY)) + ":" + StringManager.formatZero(calendar.get(Calendar.MINUTE)));
		}
		tvRemain.setText(StringManager.formatMinuteSecBySec(timer.getRemainSec()));
        
        tvTimerName.setTypeface(typeface);
        tvRemain.setTypeface(typeface);
        tvEndTime.setTypeface(typeface);
        tvMsgSyuuryouJikoku.setTypeface(typeface);

        btnPlayPause.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				timer.setRunning(!timer.isRunning());
	    		if (KoTimerService.nowMaster != null) {
	    			KoTimer koTimer = KoTimerService.nowMaster.get(position);
	    			if (koTimer.getRemainSec() <= 2) {
						KoAlarmManager.stopAlarm(koTimer.getId());
						koTimer.setRunning(false);
						koTimer.setRemainSec(koTimer.getAlarmSeconds());
						DBManager.thisInstance.update(koTimer);
						return;
					}
				}

				new DBManager(getContext(), "KoTimer", null, 1).update(timer);
		        if (!timer.isRunning()) {
					btnPlayPause.setBackgroundResource(R.drawable.img_play);
					tvEndTime.setText("--:--");
					handler.post(new Runnable() {
						public void run() {
							if (TimerListActivity.btnAddTimer != null) {
								TimerListActivity.btnAddTimer.setEnabled(true);
							}
						}
					});
				}else{
					btnPlayPause.setBackgroundResource(R.drawable.img_pause);
					tvRemain.setText(StringManager.formatMinuteSecBySec(timer.getRemainSec()));
					handler.post(new Runnable() {
						public void run() {
							if (TimerListActivity.btnAddTimer != null) {
								TimerListActivity.btnAddTimer.setEnabled(false);
							}
						}
					});
				}
			}
		});

        return view;
    }

}
