package com.apstndbonsen.kotimer.util;

import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.SoundPool;
import android.media.SoundPool.OnLoadCompleteListener;
import android.os.Vibrator;

import com.apstndbonsen.kotimer.KoTimerService;
import com.apstndbonsen.kotimer.R;
import com.apstndbonsen.kotimer.TimerListActivity;
import com.apstndbonsen.kotimer.list.KoTimer;

@SuppressLint("UseSparseArrays") 
public class KoAlarmManager {
	
	private static SoundPool sp;
	private static HashMap<Long, KoTimer> ids = new HashMap<Long, KoTimer>();
	private static TimerTask tt = null;
	private static Timer loopTimer = null;
	private static int mySoundId = 0;
	private static boolean ready = false;
	private static Notification.Builder builder = null;
	
	public static void startAlarm(long id, KoTimer koTimer, Context context){
		if (sp != null) {
			AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
			int mode = audioManager.getRingerMode();

			if (koTimer.isSound() && mode == AudioManager.RINGER_MODE_NORMAL) {
				if (ready) {
					AudioManager audio = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
			        int musicVol = audio.getStreamVolume(AudioManager.STREAM_MUSIC);
			        sp.play(mySoundId, (float) musicVol, (float) musicVol, 0, 0, 1.0F);
				}
			}else{
		        Vibrator vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
		        vibrator.vibrate(300);
			}
			if (ids.containsKey(koTimer.getId())) {
				if (koTimer.isLaunchActivity()) {
					//Activity起動
				    Intent intent = new Intent(context, TimerListActivity.class);
				    PendingIntent pendingIntent = PendingIntent.getService(context, 51, intent, PendingIntent.FLAG_UPDATE_CURRENT);
				    AlarmManager alarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
				    alarmManager.set(AlarmManager.RTC, System.currentTimeMillis(), pendingIntent);
				}else{
				}
				
				//Notification起動
				long notificationId = KoTimerService.getNotificationId(koTimer.getId());
				NotificationManager manager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
				if (builder == null) {
					manager.cancel((int)notificationId);
					builder = new Notification.Builder(context);
					builder.setContentTitle(koTimer.getAlarmName())
				    .setSmallIcon(R.drawable.kotimer_icon_200)
					.setContentText("タイマーが終了しました");
					if(koTimer.isLed()){
						builder.setLights(0xff22ffff, 250, 250);
					}
					Notification notification = builder.build();
					if(koTimer.isLed()){
						notification.flags = Notification.FLAG_SHOW_LIGHTS;
					}
					manager.notify((int)notificationId, notification);
				}
			}
		}else{
			sp = new SoundPool(1, AudioManager.STREAM_MUSIC, 0);
			mySoundId = sp.load(context, R.raw.alarm0001, 1);
			sp.setOnLoadCompleteListener(new OnLoadCompleteListener() {
				@Override
				public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
					ready = true;
				}
			});
		}
		ids.put(id, koTimer);
	}
	
	public static void stopAlarm(long id){
		ids.remove(id);
		if (ids.size() > 0) {
			sp.stop(mySoundId);
		}
	}
	
	public static void stopRingLoop(long id){
		tt.cancel();
		loopTimer.cancel();
		tt = null;
		loopTimer = null;
		ready = false;
	}
	
}
