package com.apstndbonsen.kotimer.util;

public class StringManager {

	public static String formatMinuteSecBySec(long sec){
		long fun = sec / 60;
		long byou = sec % 60;
		
		return fun + "'" + (byou < 10 ? "0"+byou:byou) + "''";
	}
	
	public static String formatZero(int value){
		return value < 10 ? "0"+value:String.valueOf(value);
	}

}
