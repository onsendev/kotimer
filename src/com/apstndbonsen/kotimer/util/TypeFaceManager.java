package com.apstndbonsen.kotimer.util;

import android.content.Context;
import android.graphics.Typeface;

public class TypeFaceManager {

	public static Typeface getLightTypeface_001(Context context) {
		Typeface typeface = Typeface.createFromAsset(context.getAssets(), "fonts/mplus-1p-light.ttf");
		context = null;
		return typeface;
	}

}
