package com.apstndbonsen.kotimer.util;

import java.lang.reflect.Field;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Typeface;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import com.apstndbonsen.kotimer.R;

public class TypeFaceSetter {


	public void setTypeFace(Activity activity){
		Typeface typeface = TypeFaceManager.getLightTypeface_001(activity);
		Class<R.id> clazz = R.id.class;
		Field[] ids = clazz.getFields();
		for (int i = 0; i < ids.length; i++) {
			Field id = ids[i];
			if (id.getName().startsWith("tv")){
				try {
					TextView tv = ((TextView)activity.findViewById(id.getInt(null)));
					tv.setTypeface(typeface);
					tv.setTextColor(Color.WHITE);
				} catch (Exception e) {
				}
			}else if (id.getName().startsWith("et")) {
				try {
					EditText et = (EditText)activity.findViewById(id.getInt(null));
					et.setTypeface(typeface);
					et.setTextColor(Color.WHITE);
				} catch (Exception e) {
				}
			}else if (id.getName().startsWith("btn")) {
				try {
					Button btn = (Button)activity.findViewById(id.getInt(null));
					btn.setTypeface(typeface);
					btn.setTextColor(Color.WHITE);
				} catch (Exception e) {
				}
			}else if(id.getName().startsWith("rb")){
				try {
					RadioButton btn = (RadioButton)activity.findViewById(id.getInt(null));
					btn.setTypeface(typeface);
					btn.setTextColor(Color.WHITE);
				} catch (Exception e) {
				}
			}else if(id.getName().startsWith("cb")){
				try {
					CheckBox btn = (CheckBox)activity.findViewById(id.getInt(null));
					btn.setTypeface(typeface);
					btn.setTextColor(Color.WHITE);
				} catch (Exception e) {
				}
			}
		}
	}

}
